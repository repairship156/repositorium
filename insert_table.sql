INSERT INTO groups (id_group, namegroup, curator, speciality) VALUES (1, 'Первая', 'Смирнов А.М.', 'Информационные технологии');
INSERT INTO groups (id_group, namegroup, curator, speciality) VALUES (2, 'Вторая', 'Петров Ю.И.', 'Программирование');

INSERT INTO students (id_student, firstname, patronymic, surname, dateofbirth, gender, educationyear, groupid)
		VALUES (1, 'Наталья', 'Андреевна', 'Чичикова', '1990-06-10', 'F', 2006, 1);

INSERT INTO students (id_student, firstname, patronymic, surname, dateofbirth, gender, educationyear, groupid)
	VALUES (2, 'Виктор', 'Сидорович', 'Белов', '1990-01-10', 'М', 2006, 1);

INSERT INTO students (id_student, firstname, patronymic, surname, dateofbirth, gender, educationyear, groupid)
	VALUES (3, 'Петр', 'Викторович', 'Сушкин', '1991-03-12', 'M', 2006, 2);
	
INSERT INTO students (id_student, firstname, patronymic, surname, dateofbirth, gender, educationyear, groupid)
	VALUES (4, 'Вероника', 'Сергеевна', 'Ковалева', '1991-04-20', 'F', 2006, 2);
	
INSERT INTO students (id_student, firstname, patronymic, surname, dateofbirth, gender, educationyear, groupid)
	VALUES (5, 'Ирина', 'Федоровна', 'Истомина', '1990-01-10', 'М', 2006, 2);