﻿CREATE TABLE groups

(   id_group integer PRIMARY KEY not null,
	nameGroup varchar(255) not null,
	curator varchar(255) not null,
	speciality varchar(255) not null);

CREATE TABLE students

(	id_student integer PRIMARY KEY not null,
	firstName varchar(255) not null,
	surName varchar(255) not null,
 	patronymic varchar(255) not null,
	dateOfBirth date not null,
	gender char(1),
	groupId integer not null,
	educationYear integer not null);


INSERT INTO groups (id_group, nameGroup, curator, speciality) values (1, 'Первая', 'доктор Борменталь', 'создание');